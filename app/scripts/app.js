'use strict';

/**
 * @ngdoc overview
 * @name uiTestApp
 * @description
 * # uiTestApp
 *
 * Main module of the application.
 */
angular
  .module('uiTestApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'angularUtils.directives.dirPagination'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .when('/addEditClient/:type/:id', {
        templateUrl: 'views/addEdit.html',
        controller: 'AddEditCtrl',

      })
      .otherwise({
        redirectTo: '/'
      });
  })
  .run(function($rootScope, $http) {
    $http.get('scripts/sampleData.json').then(function(data) {
        $rootScope.clients = data.data;                            
    });
  });
