'use strict';


angular.module('uiTestApp')
  .controller('MainCtrl', function ($scope, $http, $location, $cookies, $rootScope, $routeParams) {

    $scope.items = $rootScope.clients;


    $scope.currentPage = 1;
    $scope.pageSize = 10;
    $scope.sort = {
        sortingOrder : 'id',
        reverse : false
    };

    $scope.editClient = function (item){
        $location.path ('/addEditClient/edit/' + item.id);
    };


})
.directive('customSort', function() {
return {
    restrict: 'A',
    transclude: true,
    scope: {
      order: '=',
      sort: '='
    },
    template :
      ' <a ng-click="sortBy(order)" style="color: #555555;">'+
      '    <span ng-transclude></span>'+
      '    <i ng-class="selectedCls(order)"></i>'+
      '</a>',
      link: function(scope) {

        // change sorting order
        scope.sortBy = function(newSortingOrder) {
            var sort = scope.sort;

            if (sort.sortingOrder == newSortingOrder){
                sort.reverse = !sort.reverse;
            }

            sort.sortingOrder = newSortingOrder;
        };


        scope.selectedCls = function(column) {
            if(column == scope.sort.sortingOrder){
                return ('fa fa-sort-' + ((scope.sort.reverse) ? 'desc' : 'asc'));
            }
            else{
                return'fa fa-fw fa-sort';
            }
        };
      }// end link
  };
});

