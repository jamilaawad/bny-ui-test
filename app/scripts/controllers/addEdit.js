'use strict';

angular.module('uiTestApp')
  .controller('AddEditCtrl', function ($scope, $rootScope, $routeParams, $location, $filter, $window, _) {

  	$scope.selectedClientIndex; 
  	$scope.addClient = function (newClient){
  		if (newClient.isDefault) {
            newClient.isDefault = 'Y';
        }else{
            newClient.isDefault = 'N';
        }
        //check in DB if an item with the same properties already exists
  		if(_.findWhere($rootScope.clients, {name: newClient.name, description: newClient.description, type: newClient.type, isDefault: newClient.isDefault})){
  			$window.alert ('A client with the same data already exists');
  		}else{
  			//assumes that an ID is auto generated when new data is saved in DB 			
  			$rootScope.clients.push (newClient);
  			//should check if attempt to save in DB returns an error or success 
  			$window.alert ('Added client successfully');
  			$scope.selectedClient = {};
  		}
  		
  	};

  	$scope.findClient = function (clientID){  		
  		$scope.selectedClientIndex = $filter('getById')($rootScope.clients, clientID);
  		$scope.selectedClient = angular.copy($rootScope.clients[$scope.selectedClientIndex]);
  	};

  	$scope.deleteClient = function (clientID){
  		$scope.findClient(clientID);
  		if ($scope.selectedClientIndex != -1) {
  			$rootScope.clients.splice($scope.selectedClientIndex, 1);
  			//check if deletion returns error or success
  			$window.alert ('Deleted successfully');
  			$scope.go ('#/');
  		}
  	};

  	$scope.editClient = function(updatedClient){
  		if ($scope.selectedClientIndex != -1) {
  			if (updatedClient.isDefault == true || updatedClient.isDefault == 'Y') {
	            updatedClient.isDefault = 'Y';	            
	        }else{
	            updatedClient.isDefault = 'N';
	        }
	        //check DB for duplicates
	  		if(_.findWhere($rootScope.clients, {name: updatedClient.name, description: updatedClient.description, type: updatedClient.type, isDefault: updatedClient.isDefault}) == $rootScope.clients[$scope.selectedClientIndex]){
	  			$window.alert ('A client with the same data already exists ');
	  		}else{
				$rootScope.clients[$scope.selectedClientIndex] = updatedClient;  
				//check if update in DB was success or error 				
				$window.alert ('Updated successfully');
				$scope.go('#/');
	  			
	  		}
  		}
  	}; 

  	$scope.go = function ( path ) {
	  $location.path( path );
	};
  	  	
  	if ($routeParams.type == 'edit') {  		
  		$scope.edit = true;
  		$scope.add = false;
  		$scope.action = 'Edit Client';
  		$scope.findClient($routeParams.id);  		
  	}else{  		
  		$scope.edit = false;
  		$scope.add = true;
  		$scope.action = 'Add Client';
  	}


  })
  .filter('getById', function() {
  return function(input, id) {
    var i=0, len=input.length;
    for (; i<len; i++) {
      if (+input[i].id == +id) {
        return i;
      }
    }
    return -1;
  };
});